﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using TestProject.Models.Report;

namespace TestProject.Controllers
{
    [RoutePrefix("api/report")]
    public class ReportController : ApiController
    {
        private static string DenovoUrl => ConfigurationManager.AppSettings["DenovoUrl"];

        // GET api/report/summary
        /// <summary>
        /// Getting data for Summary Report
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>UniversalReportView object</returns>
        /// <remarks>
        /// This method will return an object with data for chart and table for general summary report.<br />
        /// Example request:<br/>
        /// {<br/>
        ///     "period": "2",<br/>
        ///     "currentDate": "2017-11-01T00:00:00+07:00"<br/>
        /// }
        /// </remarks>
        [HttpPost]
        [Route("summary")]
        [ResponseType(typeof(UniversalReportView))]
        public IHttpActionResult GetSummary(ReportParameterLiteQueryModel parameters)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var client = new HttpClient() { BaseAddress = new Uri(DenovoUrl) };
            var urlParams = "denovo/api/testProjectData/report/summary";
            try
            {
                var json = JsonConvert.SerializeObject((ReportParameterQueryModel)parameters);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(urlParams, content).Result;
                var res = response.Content.ReadAsAsync<UniversalReportView>().Result;
                return Ok(res);
            }
            catch (HttpResponseException ex)
            {
                return Content(ex.Response.StatusCode, ex.Response.RequestMessage);
            }
            catch(Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // GET api/report/card
        /// <summary>
        /// Getting data for Card Report
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>UniversalReportView object</returns>
        /// <remarks>
        /// This method will return an object with data for chart and table for payment cards report.<br />
        /// Example request:<br/>
        /// {<br/>
        ///     "period": "2",<br/>
        ///     "currentDate": "2017-11-01T00:00:00+07:00"<br/>
        /// }
        /// </remarks>
        [HttpPost]
        [Route("card")]
        [ResponseType(typeof(UniversalReportView))]
        public IHttpActionResult GetCard(ReportParameterLiteQueryModel parameters)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var client = new HttpClient() { BaseAddress = new Uri(DenovoUrl) };
            var urlParams = "denovo/api/testProjectData/report/card";
            try
            {
                var json = JsonConvert.SerializeObject((ReportParameterQueryModel)parameters);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(urlParams, content).Result;
                var res = response.Content.ReadAsAsync<UniversalReportView>().Result;
                return Ok(res);
            }
            catch (HttpResponseException ex)
            {
                return Content(ex.Response.StatusCode, ex.Response.RequestMessage);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
