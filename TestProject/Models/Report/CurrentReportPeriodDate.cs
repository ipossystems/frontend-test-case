﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestProject.Models.Report.Enums;

namespace TestProject.Models.Report
{
    public class CurrentReportPeriodDate
    {
        public DateTime CurrentDate { get; set; }
        public DateTime FirstDate { get; set; }
        public DateTime LastDate { get; set; }
        public DateTime OldFirstDate { get; set; }
        public DateTime OldLastDate { get; set; }
        public Dictionary<int, string> Periods { get; set; }
        public string StringPeriod { get; set; }
        public string DisplayBy { get; set; }
        public PeriodParameter Period { get; set; }
    }
}