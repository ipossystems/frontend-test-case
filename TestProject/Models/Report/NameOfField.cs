﻿using TestProject.Models.Report.Enums;

namespace TestProject.Models.Report
{
    public class NameOfField
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public FieldValueType Type { get; set; }
        public bool Visible { get; set; }
    }
}