﻿using System;
using System.Collections.Generic;
using System.Configuration;
using TestProject.Models.Report.Enums;

namespace TestProject.Models.Report
{
    public class ReportParameterQueryModel
    {
        public int? MerchantId { get; set; }
        public int? CompanyId { get; set; }
        public PeriodParameter Period { get; set; }
        public DateTimeOffset? CurrentDate { get; set; }
        public List<int> Locations { get; set; }
        public List<int> Merchants { get; set; }
        public List<string> Terminals { get; set; }
        public List<string> Groups { get; set; }
        public string OrderBy { get; set; }
        public bool Asc { get; set; }
        public bool TotalsOnly { get; set; }
        public string Type { get; set; }
        public List<int> Inventories { get; set; }

        public static explicit operator ReportParameterQueryModel(ReportParameterLiteQueryModel parameters)
        {
            var companyId = int.Parse(ConfigurationManager.AppSettings["DefaultCompanyId"]);
            var merchantId = int.Parse(ConfigurationManager.AppSettings["DefaultMerchantId"]);

            return new ReportParameterQueryModel
            {
                Period = parameters.Period,
                CurrentDate = parameters.Date,
                CompanyId = companyId,
                MerchantId = merchantId
            };
        }
    }
}