﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestProject.Models.Report.Enums
{
    public enum ChartViewType
    {
        Bars = 0,
        Pie = 1
    }
}