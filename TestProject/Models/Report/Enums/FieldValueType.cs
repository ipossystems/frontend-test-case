﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestProject.Models.Report.Enums
{
    public enum FieldValueType
    {
        Text = 0,
        Int = 1,
        Amount = 2,
        Link = 3,
        Percent = 4
    }
}