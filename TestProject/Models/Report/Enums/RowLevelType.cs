﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestProject.Models.Report.Enums
{
    public enum RowLevelType
    {
        Row = 0,
        Subrow = 1
    }
}