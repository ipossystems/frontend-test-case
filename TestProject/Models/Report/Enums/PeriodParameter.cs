﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestProject.Models.Report.Enums
{
    public enum PeriodParameter
    {
        Day = 0,
        Week = 1,
        Month = 2,
        Quarter = 3,
        Year = 4
    }
}