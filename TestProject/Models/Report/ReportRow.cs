﻿using System.Collections.Generic;

namespace TestProject.Models.Report
{
    public class ReportRow
    {
        public List<ReportField> Fields { get; set; }
        public List<ReportRow> SubRows { get; set; }
        public List<ReportField> OldData { get; set; }
        public List<ReportField> HostData { get; set; }
        public List<ReportField> ExtData { get; set; }
    }
}