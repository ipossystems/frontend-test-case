﻿namespace TestProject.Models.Report
{
    public class LegendViewSettingReport
    {
        public string Name { get; set; }
        public string Color { get; set; }
    }
}