﻿namespace TestProject.Models.Report
{
    public class ReportField
    {
        public int Id { get; set; }
        public object Value { get; set; }
    }
}