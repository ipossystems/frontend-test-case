﻿using System;
using System.ComponentModel.DataAnnotations;
using TestProject.Models.Report.Enums;

namespace TestProject.Models.Report
{
    public class ReportParameterLiteQueryModel
    {
        [Required]
        public PeriodParameter Period { get; set; }
        [Required]
        public DateTimeOffset Date { get; set; }
    }
}