﻿using System.Collections.Generic;

namespace TestProject.Models.Report
{
    public class ViewSettings
    {
        public int DefaultField { get; set; }
        public int? LegendField { get; internal set; }
        public int DefaultPlot { get; set; }
        public List<PlotViewSettingReport> Plots { get; set; }
    }
}