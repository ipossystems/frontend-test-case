﻿using System.Collections.Generic;
using TestProject.Models.Report.Enums;

namespace TestProject.Models.Report
{
    public class PlotViewSettingReport
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public ChartViewType Type { get; set; }
        public int LegendField { get; set; }
        public RowLevelType LegendFieldType { get; set; }
        public int ValueField { get; set; }
        public RowLevelType ValueFieldType { get; set; }
        public int DomainField { get; set; }
        public List<LegendViewSettingReport> Legends { get; set; }
    }
}