﻿using System.Collections.Generic;

namespace TestProject.Models.Report
{
    public class UniversalReportView
    {
        public CurrentReportPeriodDate PeriodInfo { get; set; }
        public List<NameOfField> FieldsInfo { get; set; }
        public List<ReportField> Totals { get; set; }
        public ViewSettings ViewSettings { get; set; }
        public List<ReportRow> Rows { get; set; }
    }
}